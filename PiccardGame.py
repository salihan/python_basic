import random

deck = 10

class Piccard:
    def __init__(self, name):
        self.name = name
        self.card = random.randint(1,deck)
        self.score = 0

    def addScore(self):
        self.score += 1
        print(f"a point is added to {self.name}")


if __name__ == '__main__':

    iRound = 0
    score_p1 = 0
    score_p2 = 0
    while iRound <5:
        iRound +=1
        p1 = Piccard(name="Player 1")
        p2 = Piccard(name="Player 2")
        print(f"Round {iRound}:")
        print(f"p1 card is: {p1.card}")
        print(f"p2 card is: {p2.card}")
        if p1.card > p2.card:
            print(f"{p1.name} win round {iRound}")
            score_p1 +=1
        elif p2.card > p1.card:
            print(f"{p2.name} win round {iRound}")
            score_p2 +=1
        else:
            print("It's a TIE!")

        print(f"Score p1: {score_p1} vs p2: {score_p2}")
        print("")
