#!/usr/bin/env python
# coding: utf-8

# # Modules

# In[1]:


import helpers


# In[4]:


fname = helpers.first_name()
lname = helpers.last_name()


# In[5]:


print(f"My name is {fname} {lname}")


# # Packages

# In[8]:


import requests


# In[15]:


# setting up a header - metadata for api request
my_header = {'Accept': 'application/json'}


# In[46]:


joke_url = 'https://icanhazdadjoke.com'

# API call
response = requests.get(joke_url, headers = my_header)


# In[47]:


joke_json = response.json()
print(type(joke_json))
joke_json


# In[50]:


print(type(joke_json['joke']))
joke_json['joke']


# # Working with files

# In[51]:


# write out the jokes into a file
with open('jokes.txt','w') as f:
    f.write(joke_json['joke'])


# In[52]:


# read from a file
with open('jokes.txt','r') as f:
    fjokes = f.read()
fjokes


# # Error handling

# In[54]:


# create a new set
new_set = {1,2,3,4,5}
# create a new list
new_list = [1,2,3,4,5]


# In[56]:


print(type(new_set))
print(type(new_list))


# In[58]:


try:
    new_set[0] = 4
except Exception as  e:
    print(e)
    print("objek 'set' tidak menyokong penugasan item")


# In[60]:


try:
    new_list[0] = 4
    print(new_list)
except Exception as  e:
    print(e)
    print("objek 'set' tidak menyokong penugasan item")


# In[ ]:




