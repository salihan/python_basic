# conventional approach:

#initialise var
# totalexp = 0
# exp = -1
# maxexp = 0
# counter = 0
#
# while not exp == 0:
#     counter += 1
#     exp = int(input("Enter expense amount or '0' to get the total: "))
#     totalexp = totalexp + exp
#
#     if counter == 1:
#         minexp = exp
#         maxexp = exp
#     else:
#         if exp > maxexp:
#             maxexp = exp
#         if minexp > exp and exp != 0:
#             minexp = exp
#
# print(f"Total spent: {totalexp}")
# print(f"Biggest spent: {maxexp}")
# print(f"Smallest spent: {minexp}")
# print(f"Average spent: {totalexp / counter}")

# ---------------------------------------------------------------------------

# Using list approach:

exp = []
inp = 0

while True:
    inp = int(input("Enter expense amount or '0' to get the total: "))
    exp.append(inp)
    if inp == 0:
        break
exp.pop()
print(f"Expense list: {exp}")
print(f"Biggest expense: {max(exp)}")
print(f"Lowest expense: {min(exp)}")
print(f"Average expenses: {sum(exp) / len(exp)}")
