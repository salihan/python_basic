import math

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.

def tahun_lahir():
    current_year = 2022
    umur = input("What is your age?")
    print("Your year of birth is ", current_year - int(umur))
    print(f"Next year you will be {int(umur) + 1}")

def looper():
    friends = ["Ahmad", "Nisfu", "Jais"]
    best_friend = "Nisfu"
    for friend in friends:
        print(f"{friend} is my friend")
        if friend == best_friend :
            print(f"{best_friend} is my best friend.")

def buy_sandwich():
    budget = 100
    sandwich_price = 5

    quantity = 0
    while budget > 0:
        quantity+=1
        print(f"budget:{int(budget)}. Quantity: {int(quantity)}")
        budget = budget - sandwich_price

def elevator():
    tingkat = 0
    while True:
        tingkat = input("Push floor nombor or 'out' to exit: ")
        if tingkat == "out":
            print("babai!")
            break
        else:
            if int(tingkat) in range(1,21):
                print(f"welcome to floor {tingkat}")
            else:
                print(f"I cannot take u to the {tingkat} floor ")

def pitagoras(a, b, c):
    ab = a**2 + b**2
    ci = c**2

    if ab == ci:
        print(f"The combination of {a}, {b} and {c} follows the pyth law.")
    else:
        print(f"The combination of {a}, {b} and {c} does NOT follows the pyth law.")


class CreditCard:
    def __init__(self, number, limit=5000):
        self.number = number
        self.limit = limit

    def hide_crd(self):
        last4digitno = self.number[-4:]
        return f"**** **** **** {last4digitno}"

    limit_rate = 1.5
    def setnew_limit(self):
        self.limit = CreditCard.limit_rate * self.limit
        print(f"New limit: {self.limit} has been set for the card {self.number}")

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    #print_hi('PyCharm')
    #tahun_lahir()
    #print(math.ceil(3.3))
    #looper()
    #buy_sandwich()
    #elevator()
    # pitagoras(3, 4, 5)
    # pitagoras(5, 6, 7)
    c1 = CreditCard(number='1234567890123456')
    c2 = CreditCard(number='1245635278596585', limit=3000)
    print(c1.number, c1.limit)
    c1.limit = 6000
    print(c1.hide_crd())
    print(c1.limit)
    c1.setnew_limit()
    c2.setnew_limit()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
