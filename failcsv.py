from datetime import date
import csv

dt = date.today().strftime("%d/%m/%y")
filename = "test.csv"
exp = []
bloop = True

with open(filename, 'a', newline="") as file:
    csvwriter = csv.writer(file)
    while bloop:
        userinput = int(input("Enter expense or '0' to finish: "))
        if userinput == 0:
            bloop = False
        else:
            csvwriter.writerow([dt, userinput])
            exp.append(userinput)
file.close()
print(f"The user input: {exp}")
print(f"Biggest expense: {max(exp)}")
print(f"Lowest expense: {min(exp)}")
print(f"Average expenses: {sum(exp) / len(exp)}")